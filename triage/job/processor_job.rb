# frozen_string_literal: true

require_relative '../triage/error'
require_relative '../triage/event'
require_relative '../triage/handler'
require_relative '../triage/job'
require_relative '../triage/sentry'
require_relative '../triage'

module Triage
  class ProcessorJob < Job
    workers 16 # Puma has default concurrency set at min:0 max:16

    private

    def execute(payload)
      prepare_executing_with(Triage::Event.build(payload))

      handler = Handler.new(event)

      start_time = Triage.current_monotonic_time
      results = handler.process

      capture_and_log_errors(results)

      messages = results.transform_values(&:message).compact

      ::Rack::Response.new([JSON.dump(status: :ok, messages: messages)]).finish
    rescue Triage::Event::UnsupportedObjectKind => e
      log_warning(e)
      error_response(e)
    rescue Triage::ClientError => e
      log_error(e)
      error_response(e)
    rescue StandardError => e
      log_error(e)
      capture_error(e)
      error_response(e, status_code: 500)
    rescue Exception => e # rubocop:disable Lint/RescueException
      log_error(e)
      capture_error(e)
      # We don't want to interrupt the flow (for example, signal handling),
      # so we just re-raise whatever coming by, but we would still want to
      # know what happened if there's something going through.
      raise
      # Since we're re-raising the exception, we don't need to return anything
    ensure
      log_request_processing(results, start_time) if results
    end

    def log_request_processing(results, start_time)
      logger.info(
        'Event processing',
        dry_run: Triage.dry_run?,
        event_class: event.class.to_s,
        event_key: event.key,
        event_payload: event.payload,
        results: results_for_logs(results),
        duration: (Triage.current_monotonic_time - start_time).round(5)
      )
    end

    def results_for_logs(results)
      results.each_with_object([]) do |(processor, result), memo|
        memo << { processor: processor, **result.to_h }
      end
    end

    def error_response(error, status_code: 400)
      # We're always responding with HTTP 200 here, because otherwise
      # we might make ourselves disabled from getting more webhook events.
      # See: https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/921#note_875934104
      #
      # * If we give 500 error, we'll be temporarily disabled for 10 minutes
      #   as the initial backoff, and double to 20 minutes upon next
      #   consecutive 500 error.
      # * If we give 400 error 3 consecutive times, we'll be disabled
      #   permanently.
      actual_http_status = 200

      ::Rack::Response.new([JSON.dump(status: :error, status_code: status_code, error: error.class, message: error.message)], actual_http_status).finish
    end

    def capture_and_log_errors(results)
      results.each do |processor, result|
        next unless result.error

        log_error(result.error, processor)
        capture_error(result.error, processor)
      end
    end

    def capture_error(error, processor = nil)
      # Event can be nil because it may fail to build and we reach here to
      # log the error that it failed to build the event.
      Raven.tags_context(
        event_class: event&.class.to_s, event_key: event&.key,
        service: 'reactive')
      Raven.extra_context(processor: processor, payload: event&.payload)
      Raven.capture_exception(error)
    end

    def log_warning(msg, processor = nil)
      log_entry(:warn, msg, processor)
    end

    def log_error(msg, processor = nil)
      log_entry(:error, msg, processor)
    end

    def log_entry(type, msg, processor = nil)
      # Event can be nil because it may fail to build and we reach here to
      # log the error that it failed to build the event.
      logger.__send__(type,
        msg,
        processor: processor,
        event_class: event&.class.to_s,
        event_key: event&.key,
        event_payload: event&.payload)
    end
  end
end
