# frozen_string_literal: true

require_relative 'changed_file_list'

module Triage
  class Change
    def self.detect(changed_file_list)
      changed_file_list.to_enum(:each_change, file_patterns).filter_map do |change|
        new(change) if change['diff'].match?(line_patterns)
      end
    end

    def self.file_patterns
      raise NotImplementedError
    end

    def self.line_patterns
      //
    end

    def initialize(new_change)
      self.change = new_change
    end

    def ==(other)
      self.class == other.class &&
        change.to_h == other.change.to_h
    end

    def path
      change['new_path']
    end

    def type
      self.class.name[/[^:]+$/]
    end

    protected

    attr_accessor :change
  end
end
