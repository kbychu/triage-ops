# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative 'triage_incident'

module Triage
  module PipelineFailure
    class IncidentCreator
      def initialize(event:, config:, failed_jobs:)
        @event = event
        @config = config
        @failed_jobs = failed_jobs
      end

      def execute
        payload = {
          issue_type: 'incident',
          description: format(config.incident_template, template_variables),
          labels: config.incident_labels,
          **config.incident_extra_attrs
        }

        payload[:labels].push(triager.top_root_cause_label) if config.auto_triage?
        payload[:labels].push(triager.top_group_label) if config.auto_triage? && triager.top_group_label.present?

        incident = Triage.api_client.create_issue(config.incident_project_id, title, payload).tap do |incident|
          Triage.api_client.post(
            incident_discussion_path(incident),
            body: { body: root_cause_analysis_body })
          Triage.api_client.post(
            incident_discussion_path(incident),
            body: { body: investigation_body })
        end

        return incident unless close_incident?

        # #close_issue returns the up-to-date incident
        Triage.api_client.close_issue(config.incident_project_id, incident.iid)
      end

      private

      attr_reader :event, :config, :failed_jobs

      def now
        @now ||= Time.now.utc
      end

      def title
        @title ||= begin
          full_title = "#{now.strftime('%A %F %R UTC')} - `#{event.project_path_with_namespace}` " \
                       "broken `#{event.ref}` with #{failed_jobs.map(&:name).join(', ')}"

          if full_title.size >= 255
            "#{full_title[...252]}..." # max title length is 255, and we add an elipsis
          else
            full_title
          end
        end
      end

      def template_variables
        {
          project_link: project_link,
          pipeline_link: pipeline_link,
          branch_link: branch_link,
          commit_link: commit_link,
          triggered_by_link: triggered_by_link,
          source: source,
          pipeline_duration: pipeline_duration,
          failed_jobs_count: failed_jobs.size,
          failed_jobs_list: failed_jobs_list,
          merge_request_link: merge_request_link,
          attribution_body: attribution_body
        }
      end

      def project_link
        "[#{event.project_path_with_namespace}](#{event.project_web_url})"
      end

      def incident_discussion_path(incident)
        "/projects/#{config.incident_project_id}/issues/#{incident.iid}/discussions"
      end

      def pipeline_link
        "[##{event.id}](#{event.web_url})"
      end

      def branch_link
        "[`#{event.ref}`](#{event.project_web_url}/-/commits/#{event.ref})"
      end

      def commit_link
        "[#{event.commit_header}](#{event.project_web_url}/-/commit/#{event.sha})"
      end

      def triggered_by_link
        # Recreate the server URL from event.project_web_url...
        "[#{event.event_actor.name}](#{event.project_web_url.delete_suffix(event.project_path_with_namespace)}#{event.event_actor.username})"
      end

      def source
        "`#{event.source}`"
      end

      def pipeline_duration
        ((now - event.created_at) / 60.to_f).round(2)
      end

      def failed_jobs_list
        failed_jobs.map { |job| "- [#{job.name}](#{job.web_url}) **Job ID**: `#{job.id}`" }.join("\n")
      end

      def merge_request_link
        return 'N/A' unless event.merge_request

        "[#{event.merge_request.title}](#{event.merge_request.web_url})"
      end

      def root_cause_analysis_body
        body = "## Root Cause Analysis"
        body += triager.root_cause_analysis_comment if config.auto_triage?

        body
      end

      def investigation_body
        body = "## Investigation Steps"
        body += triager.investigation_comment if config.auto_triage? && triager.investigation_comment

        body
      end

      def attribution_body
        return unless config.auto_triage? && triager.attribution_comment

        <<~MARKDOWN
            ### Attribution

            If RSpec tests failed, the group having most of the failing tests is assigned to the incident.

            Please note that if the assigned group is wrong, the tests may be tagged with the wrong feature_category metadata.
            You can follow the [guide](https://docs.gitlab.com/ee/development/feature_categorization/#rspec-examples) to
            update it so future incidents will be labelled with the correct group.

            ~"Engineering Productivity" will be added if no group label is identified.

            #{triager.attribution_comment}
        MARKDOWN
      end

      def close_incident?
        config.auto_triage? && triager.closeable?
      end

      def triager
        @triager ||= TriageIncident.new(
          event: event,
          config: config,
          failed_jobs: failed_jobs
        )
      end
    end
  end
end
