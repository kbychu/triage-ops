# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class DevMasterBranch < Base
        INCIDENT_PROJECT_ID = '5064907' # gitlab-org/release/tasks
        INCIDENT_LABELS = ['release-blocker'].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN
          ## %<project_link>s pipeline %<pipeline_link>s failed

          **Branch: %<branch_link>s**

          **Commit: %<commit_link>s**

          **Merge Request: %<merge_request_link>s**

          **Triggered by** %<triggered_by_link>s • **Source:** %<source>s • **Duration:** %<pipeline_duration>s minutes

          **Failed jobs (%<failed_jobs_count>s):**

          %<failed_jobs_list>s

          ### General guidelines

          A broken `master` branch on dev prevents monthly releases from being built.
          Fixing the pipeline is a priority to prevent any delays in releases.

          The process in the [Broken `master` handbook guide](https://about.gitlab.com/handbook/engineering/workflow/#broken-master) can be referenced since much of that process also applies here.

          ### Investigation

          **Be sure to fill the `Timeline` for this incident.**

          1. If the failure is new, and looks like a potential flaky failure, you can retry the failing job.
            Make sure to mention the retry in the `Timeline` and leave a link to the retried job.
          1. Search for similar master-broken issues in https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/issues
            1. If one exists, ask the DRI of the master-broken issue to cherry-pick any resulting merge requests into the stable branch

          If the merge request author or maintainer is not available, this can be escalated using the dev-on-call process in the [#dev-escalation slack channel](https://gitlab.slack.com/archives/CLKLMSUR4).

          ### Pre-resolution

          If you believe that there's an easy resolution by either:

          - Reverting a particular merge request.
          - Making a quick fix (for example, one line or a few similar simple changes in a few lines).
            You can create a merge request, assign to any available maintainer, and ping people that were involved/related to the introduction of the failure.
            Additionally, a message can be posted in `#backend_maintainers` or `#frontend_maintainers` to get a maintainer take a look at the fix ASAP.
          - Cherry picking a change that was used to fix a similar master-broken issue.

          ### Resolution

          Add a comment to this issue describing how this incident could have been prevented earlier in the Merge Request pipeline (rather than the merge commit pipeline).
        MARKDOWN
        SLACK_CHANNEL = 'master-broken-mirrors'

        def self.match?(event)
          event.on_instance?(:dev) &&
            event.project_path_with_namespace == 'gitlab/gitlab-ee' &&
            !event.merge_request_pipeline? &&
            event.ref == 'master' &&
            event.source_job_id.nil?
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          INCIDENT_LABELS
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end
      end
    end
  end
end
