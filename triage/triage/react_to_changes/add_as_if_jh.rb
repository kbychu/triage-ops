# frozen_string_literal: true

require_relative '../change/dependency_change'
require_relative '../change/feature_flag_config_change'

module Triage
  class AddAsIfJH
    INTERESTED_CHANGES = [
      DependencyChange,
      FeatureFlagConfigChange
    ].freeze

    attr_reader :processor

    def initialize(new_processor)
      @processor = new_processor
    end

    def applicable?
      INTERESTED_CHANGES.all? do |change|
        change.detect(processor.changed_file_list).any?
      end
    end

    def react
      processor.add_comment(
        '/label ~"pipeline:run-as-if-jh"',
        append_source_link: false)
    end
  end
end
