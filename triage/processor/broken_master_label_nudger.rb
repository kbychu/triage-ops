# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/constants/labels'

module Triage
  class BrokenMasterLabelNudger < Processor
    react_to 'incident.close'

    UNDETERMINED_ROOT_CAUSE_LABEL = 'master-broken::undetermined'
    MASTER_BROKEN_FLAKY_TEST_LABEL = 'master-broken::flaky-test'
    FLAKY_TEST_LABEL_PREFIX = 'flaky-test::'
    ROOT_CAUSE_LABEL_MISSING_MESSAGE = 'please add a concrete `~master-broken::` root cause label'
    FLAKY_REASON_LABEL_MISSING_MESSAGE = 'please identify the flaky root cause with a `~flaky-test::` label'

    def applicable?
      event.from_master_broken_incidents_project? &&
        has_master_broken_label? &&
        !event.gitlab_bot_event_actor? &&
        (need_root_cause_label? || need_flaky_reason_label?)
    end

    def process
      add_comment(comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor reminds team member to provide concrete master-broken root cause label or flaky test reason label.
      TEXT
    end

    private

    def has_master_broken_label?
      event.label_names.include?(Labels::MASTER_BROKEN_LABEL) || event.label_names.include?(Labels::MASTER_FOSS_BROKEN_LABEL)
    end

    def need_root_cause_label?
      event.label_names.include?(UNDETERMINED_ROOT_CAUSE_LABEL)
    end

    def need_flaky_reason_label?
      event.label_names.include?(MASTER_BROKEN_FLAKY_TEST_LABEL) && !has_flaky_reason_label?
    end

    def has_flaky_reason_label?
      event.label_names.any? { |label| label.start_with? FLAKY_TEST_LABEL_PREFIX }
    end

    def comment
      message = need_root_cause_label? ? ROOT_CAUSE_LABEL_MISSING_MESSAGE : FLAKY_REASON_LABEL_MISSING_MESSAGE

      message_markdown = <<~MARKDOWN.chomp
        :wave: @#{event.event_actor_username}, #{message} following the [Triage DRI Responsibilities handbook page](https://about.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities).

        Ignore this comment if you think the incident already has all of the necessary labels to conclude your root cause analysis.
      MARKDOWN

      unique_comment.wrap(message_markdown).strip
    end
  end
end
