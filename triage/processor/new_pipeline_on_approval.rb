# frozen_string_literal: true

require_relative '../job/trigger_pipeline_on_approval_job'
require_relative '../triage/processor'
require_relative '../triage/event'
require_relative '../triage/unique_comment'
require_relative '../triage/changed_file_list'
require_relative 'label_jihu_contribution'
require_relative '../../lib/constants/labels'

module Triage
  class NewPipelineOnApproval < Processor
    SKIP_WHEN_CHANGES_ONLY_REGEX = %r{\A(?:docs?|qa|\.gitlab/(issue|merge_request)_templates)/}
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'
    FIVE_SECONDS = 5
    SUPPORTED_PROJECT_IDS = [
      Triage::Event::GITLAB_PROJECT_ID,
      Triage::Event::TF_PROVIDER_GITLAB_PROJECT_ID
    ].freeze
    NewPipelineMessage = Struct.new(:event, :trigger_pipeline_automatically?) do
      def to_s
        <<~MARKDOWN.strip
          #{header}

          #{body}

          #{references}

          /label ~"#{Labels::MR_APPROVED_LABEL}"
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
          :wave: `@#{event.event_actor_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request has been approved. #{call_to_action}
        BODY
      end

      def references
        <<~REF.chomp
          For more info, please refer to the following links:
          - [`rspec` predictive jobs](https://docs.gitlab.com/ee/development/pipelines/index.html#rspec-predictive-jobs)
          - [`jest` predictive jobs](https://docs.gitlab.com/ee/development/pipelines/index.html#jest-predictive-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        REF
      end

      def call_to_action
        if trigger_pipeline_automatically?
          'To ensure full test coverage, a new pipeline will be started shortly.'
        else
          'To ensure full test coverage, please start a new pipeline before merging.'
        end
      end
    end

    react_to_approvals

    def applicable?
      SUPPORTED_PROJECT_IDS.include?(event.project_id) &&
        need_new_pipeline? &&
        unique_comment.no_previous_comment?
    end

    def process
      trigger_pipeline_automatically =
        event.team_member_author? || event.automation_author?

      message = unique_comment.wrap(
        NewPipelineMessage.new(event, trigger_pipeline_automatically))

      if trigger_pipeline_automatically
        add_comment(message, append_source_link: false)

        trigger_merge_request_pipeline
      else
        add_discussion(message, append_source_link: false)
      end
    end

    def documentation
      <<~TEXT
        This processor triggers a pipeline run on a newly approved merge request.
      TEXT
    end

    private

    def need_new_pipeline?
      !expedited_mr? &&
        !event.source_branch_is?(UPDATE_GITALY_BRANCH) &&
        !event.target_branch_is_stable_branch? &&
        !changed_file_list.only_change?(SKIP_WHEN_CHANGES_ONLY_REGEX)
    end

    def expedited_mr?
      event.label_names.include?(Labels::PIPELINE_EXPEDITE_LABEL)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def trigger_merge_request_pipeline
      TriggerPipelineOnApprovalJob.perform_in(FIVE_SECONDS, event.noteable_path)
    end
  end
end
