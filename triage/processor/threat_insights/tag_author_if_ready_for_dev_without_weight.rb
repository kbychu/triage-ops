# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  class TagAuthorIfReadyForDevWithoutWeight < Processor
    def documentation
      <<~TEXT
      This processor notifies the author when the updated issue is moved to
      'workflow::ready for development' if the issue does not have a weight set.
      TEXT
    end

    react_to 'issue.update'

    def applicable?
      from_gitlab_org? &&
        ready_for_development? &&
        group_threat_insights? &&
        not_bug_or_spike? &&
        weight_missing? &&
        frontend_or_backend? &&
        resource_open?
    end

    def process
      add_comment(message, append_source_link: false)
    end

    private

    def message
      <<~MARKDOWN.chomp
        @#{event.event_actor_username}, please add the [weight as per the documentation](https://about.gitlab.com/handbook/engineering/development/sec/govern/sp-ti-planning.html#weights).

        ~"group::threat insights" issues require a weight when labelled ~"workflow::ready for development". If indeed this issue does not need a weight, then it must be either (labelled as a ~spike or ~bug), or (unlabelled as ~frontend and ~backend).
      MARKDOWN
    end

    def label_names
      event.label_names
    end

    def from_gitlab_org?
      event.from_gitlab_org?(sandbox: true)
    end

    def ready_for_development?
      label_names.include?(Labels::WORKFLOW_READY_FOR_DEVELOPMENT_LABEL)
    end

    def group_threat_insights?
      label_names.include?(Labels::THREAT_INSIGHTS_GROUP_LABEL)
    end

    def not_bug_or_spike?
      !(label_names.include?('type::bug') || label_names.include?('spike'))
    end

    def weight_missing?
      event.weight.nil?
    end

    def frontend_or_backend?
      label_names.include?('frontend') || label_names.include?('backend')
    end

    def resource_open?
      event.resource_open?
    end
  end
end
