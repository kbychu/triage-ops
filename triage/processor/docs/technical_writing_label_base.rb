# frozen_string_literal: true

require_relative '../../triage'
require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'

module Triage
  module Docs
    class TechnicalWritingLabelBase < Processor
      TECHNICAL_WRITING_LABEL = 'Technical Writing'

      def applicable?
        event.from_gitlab_org? && technical_writing_label_absent? && actor_is_in_docs_team? && unique_comment.no_previous_comment?
      end

      def process
        add_comment(unique_comment.wrap(comment).strip, append_source_link: true)
      end

      def documentation
        raise NotImplementedError
      end

      private

      def technical_writing_label_absent?
        !event.label_names.include?(TECHNICAL_WRITING_LABEL)
      end

      def actor_is_in_docs_team?
        Triage.gitlab_docs_team_member_ids.include?(event.event_actor_id)
      end

      def comment
        <<~MARKDOWN.chomp
          Hi `@#{event.event_actor_username}` :wave:,

          GitLab Bot has added the ~"Technical Writing" label because a Technical Writer has
          [approved or merged](https://about.gitlab.com/handbook/product/ux/technical-writing/#review-workflow)
          this MR.

          /label ~"#{TECHNICAL_WRITING_LABEL}"
        MARKDOWN
      end

      def unique_comment
        @unique_comment ||= UniqueComment.new("Triage::Docs::TechnicalWritingLabel", event)
      end
    end
  end
end
