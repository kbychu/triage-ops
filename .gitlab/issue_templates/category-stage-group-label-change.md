## Summary

This template is for the following types of label changes:

* Adding a new Category, Stage or Group
* Renaming an existing Category, Stage or Group

See [the main process in the handbook](https://about.gitlab.com/handbook/product/categories/#changes).

For every case above, please ensure that:

* [ ] The label change is factored into the triage mechanism with updated [spec fixtures](https://gitlab.com/gitlab-org/quality/triage-ops/-/tree/master/spec/fixtures).
* [ ] Old labels are migrated correctly on affected issues, merge requests and epics.

### Action items for issue author
* [ ] Provide link to the merge request in [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) for the label change.
* [ ] (If applicable) One-off label migration:
  * [ ] Refer to the [label migration documentation](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/workflow-automation/#one-off-label-migrations) to determine whether you need to run a one-off label migration, and to self-serve the migration task if possible. If you don't feel the documentation is sufficient to enable you to self-serve this request, indicate in the issue what is missing and we will improve.
* [ ] (If applicable) Archive the old label with renaming and adding "DEPRECATED" at the end of the label name.
* [ ] (If applicable) Update the label's description with link to the updated handbook page.
* [ ] (If applicable) Update the [group-definition.yml](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/group-definition.yml) with the new group name.

### Action items for Engineering Productivity

* [ ] Ensure the issue author has completed the action item checklist.
* [ ] Address any feedback left for the label migration documentation for improvements.

/cc @gl-quality/eng-prod @gitlab-org/quality/engineering-analytics
/label ~Quality ~"label change"
