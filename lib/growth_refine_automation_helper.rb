# frozen_string_literal: true

require 'httparty'
require 'addressable'
require_relative 'constants/labels'
require_relative '../triage/triage'

module GrowthRefineAutomationHelper
  GITLAB_GROUP_API_ENDPOINT = 'https://gitlab.com/api/v4/groups/9970/'
  GROWTH_LABELS = Addressable::URI.escape(Labels::GROWTH_TEAM_LABELS.join(','))
  REFINE_LABEL = Labels::WORKFLOW_REFINEMENT_LABEL
  PLAN_LABEL = Addressable::URI.escape(Labels::WORKFLOW_PLANNING_BREAKDOWN_LABEL)
  GROWTH_KANBAN_BOARD_ID = '4152639'
  COMMENT_HEADING = '## :construction: Refinement'
  VALID_REACTIONS = { 'one' => 1, 'two' => 2, 'three' => 3, 'five' => 5, 'rocket' => 0 }.freeze
  BLOCKING_REACTION = 'x'
  MIN_REACTIONS = 3

  def refinement_completed?
    return false if refinement_thread.nil?

    reactions = api_get(award_emoji_on_comment_path)

    unique_reactions_count = count_unique_reactions(reactions)

    completed = unique_reactions_count >= MIN_REACTIONS

    if completed && !dry_run?
      add_checkmark_to_refinement_thread(award_emoji_on_comment_path)
      max_weight = max_weight_of_reactions(reactions)
      update_issue_weight(max_weight)
    end

    completed
  end

  def qualify_for_refine?
    qualified_issues_id.include?(resource['iid'])
  end

  private

  def token_header
    @token_header ||= { headers: { 'Private-Token': Triage.api_token(:com) } }.freeze
  end

  def max_weight_of_reactions(reactions)
    weights = reactions.map { |reaction| VALID_REACTIONS.fetch(reaction['name'], 0) }
    weights.max
  end

  def count_unique_reactions(reactions)
    return 0 if reactions.any? { |reaction| reaction['name'] == BLOCKING_REACTION }

    unique_user_ids = []
    unique_reactions_count = 0
    reactions&.each do |reaction|
      unless unique_user_ids.include?(reaction.dig('user', 'id'))
        unique_user_ids << reaction.dig('user', 'id')
        unique_reactions_count += 1
      end
    end

    unique_reactions_count
  end

  def dry_run?
    ENV.fetch('DRY_RUN', '0').match?(/yes|1|true/i)
  end

  def api_get(path)
    response = HTTParty.get(path, token_header)
    return JSON.parse(response.body) if response.code == 200
  end

  def comments_path
    @comments_path ||= resource.dig(:_links, :notes)
  end

  def find_refinement_thread
    comments = api_get(comments_path)

    comments&.find do |comment|
      comment.fetch('body', '').start_with?(COMMENT_HEADING)
    end
  end

  def refinement_thread
    @refinement_thread ||= find_refinement_thread
  end

  def award_emoji_on_comment_path
    @award_emoji_on_comment_path ||= "#{comments_path}/#{refinement_thread['id']}/award_emoji"
  end

  def add_checkmark_to_refinement_thread(path)
    options = token_header.merge({ body: { name: 'white_check_mark' } })
    HTTParty.send(:post, path, options)
  end

  def issue_path
    resource.dig(:_links, :self)
  end

  def group_path_to(resources_path)
    GITLAB_GROUP_API_ENDPOINT + resources_path
  end

  def update_issue_weight(weight)
    return if weight.zero?

    options = token_header.merge({ body: { weight: weight } })
    HTTParty.send(:put, issue_path, options)
  end

  def refinement_max_limit
    kanban_board_lists = api_get(group_path_to("boards/#{GROWTH_KANBAN_BOARD_ID}"))['lists']
    refinement_list = kanban_board_lists.find { |list| list.dig('label', 'name') == REFINE_LABEL }
    refinement_list['max_issue_count']
  end

  def current_refine_issue_count
    res = api_get(group_path_to("issues_statistics?labels=#{REFINE_LABEL},#{GROWTH_LABELS}"))
    res.dig('statistics', 'counts', 'opened')
  end

  def find_qualified_issues
    slots_available = refinement_max_limit - current_refine_issue_count
    return [] if slots_available <= 0

    path = group_path_to("issues?per_page=#{slots_available}&state=opened&order_by=relative_position&sort=asc&labels=#{PLAN_LABEL},#{GROWTH_LABELS}")
    issues = api_get(path)
    issues.map { |issue| issue['iid'] }
  end

  def qualified_issues_id
    # rubocop:disable Style/ClassVars
    @@qualified_issues_id ||= find_qualified_issues
    # rubocop:enable Style/ClassVars
  end
end
