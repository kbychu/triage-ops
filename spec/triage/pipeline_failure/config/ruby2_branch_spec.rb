# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/ruby2_branch'

RSpec.describe Triage::PipelineFailure::Config::Ruby2Branch do
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:ref) { 'ruby2' }
  let(:source_job_id) { nil }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      ref: ref,
      source_job_id: source_job_id)
  end

  before do
    allow(event).to receive(:on_instance?).with(:com).and_return(true)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when event is not on the .com instance' do
      before do
        allow(event).to receive(:on_instance?).with(:com).and_return(false)
      end

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when project_path_with_namespace is not "gitlab-org/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when ref is not "ruby2"' do
      let(:ref) { 'foo' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when source_job_id is present' do
      let(:source_job_id) { '42' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#default_slack_channels' do
    it 'returns expected channels' do
      expect(subject.default_slack_channels).to eq(['backend'])
    end
  end
end
