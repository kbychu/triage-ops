# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/scheduled_issue_validator'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::ScheduledIssueValidator do
  include_context 'with event', Triage::IssueEvent do
    let(:milestone_id)         { 123 }
    let(:milestone_path)       { "/projects/#{project_id}/milestones/#{milestone_id}" }
    let(:issue_notes_api_path) { "/projects/#{project_id}/issues/#{iid}/notes" }
    let(:issue_path)           { "/projects/#{project_id}/issues/#{iid}" }
    let(:date_today)           { Date.today }
    let(:today)                { date_today.to_s }
    let(:in_30_days)           { (date_today + 30).to_s }
    let(:in_90_days)           { (date_today + 90).to_s }
    let(:unique_comment)       { nil }
    let(:product_project?)     { true }
    let(:label_names)          { ['not a type label'] }

    let(:event_attrs) do
      { from_part_of_product_project?: product_project?,
        milestone_id: milestone_id }
    end

    let(:milestone_attr_base) do
      { 'id' => milestone_id,
        'project_id' => project_id,
        'start_date' => today,
        'due_date' => in_30_days }
    end

    let(:milestone_attr) { milestone_attr_base }

    let(:issue_attr) do
      { 'iid' => iid, 'milestone' => milestone_attr, 'labels' => label_names }
    end

    let(:issue) { Triage::Issue.new(issue_attr) }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: issue_notes_api_path,
      query: { per_page: 100 },
      response_body: [unique_comment].compact
    )

    stub_api_request(path: milestone_path, response_body: milestone_attr)
    stub_api_request(path: issue_path, response_body: issue_attr)
  end

  describe '#type_label_nudge_needed?' do
    shared_examples 'applicable' do
      it 'returns true' do
        expect(subject.type_label_nudge_needed?).to be true
      end
    end

    shared_examples 'not applicable' do
      it 'returns false' do
        expect(subject.type_label_nudge_needed?).to be false
      end
    end

    context 'with contextual event' do
      it_behaves_like 'applicable'
    end

    context 'when issue has a type label, and all other conditions are satisifed' do
      let(:label_names) { ['type::feature'] }

      it_behaves_like 'not applicable'
    end

    context 'when issue has a type::ignore label, and all other conditions are satisifed' do
      let(:label_names) { ['type::ignore'] }

      it_behaves_like 'not applicable'
    end

    context 'when issue has a special issue label, and all other conditions are satisifed' do
      let(:label_names) { ['support request'] }

      it_behaves_like 'not applicable'
    end

    context 'when issue is not from part of product project, and all other conditions are satisfied' do
      let(:product_project?) { false }

      it_behaves_like 'not applicable'
    end

    context 'when there is a previous type label nudge comment, and all other conditions are satisfied' do
      let(:unique_comment) do
        { body: Triage::UniqueComment.new('Triage::ScheduledIssueTypeLabelNudger', event).wrap("Posted") }
      end

      it_behaves_like 'not applicable'
    end

    context 'when issue milestone is not applicable, and all other conditions are satisifed' do
      context 'with the triggering event has a nil milestone_id attribute' do
        let(:milestone_id) { nil }
        let(:milestone_attr) { nil }

        it_behaves_like 'not applicable'
      end

      context 'with the issue has a nil milestone attribute' do
        let(:milestone_attr) { nil }

        it_behaves_like 'not applicable'
      end

      context 'when the issue is assigned to a milestone due in the past' do
        let(:in_the_past) { (date_today - 90).to_s }
        let(:yesterday)   { (date_today - 1).to_s }
        let(:milestone_attr) do
          milestone_attr_base.merge({ 'start_date' => in_the_past, 'due_date' => yesterday })
        end

        it_behaves_like 'not applicable'
      end

      context 'when the issue is assigned to a milestone starting in more than 90 days' do
        let(:in_120_days) { (date_today + 120).to_s }
        let(:in_150_days)   { (date_today + 150).to_s }
        let(:milestone_attr) do
          milestone_attr_base.merge({ 'start_date' => in_120_days, 'due_date' => in_150_days })
        end

        it_behaves_like 'not applicable'
      end

      context 'when the issue is assigned to a milestone with no start date or due date' do
        let(:milestone_attr) do
          milestone_attr_base.merge({ 'start_date' => nil, 'due_date' => nil })
        end

        it_behaves_like 'not applicable'
      end
    end

    context 'when issue milestone is applicable, and all other conditions are met' do
      context 'with milestone in progress' do
        it_behaves_like 'applicable'
      end

      context 'with milestone starting in 30 days' do
        let(:milestone_attr) do
          milestone_attr_base.merge({ 'start_date' => in_30_days, 'due_date' => in_90_days })
        end

        it_behaves_like 'applicable'
      end
    end

    context 'with exempted project id' do
      let(:project_id) { '443787' }

      it_behaves_like 'not applicable'
    end
  end
end
