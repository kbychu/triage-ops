# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/reaction'

RSpec.describe Triage::Reaction do
  let(:path) { '/a/path' }
  let(:noteable_path) { '/a/noteable/path' }
  let(:text) { 'a text' }
  let(:discussion_id) { 1 }
  let(:note_id) { 999 }
  let(:discussion_path) { "#{noteable_path}/discussions/#{discussion_id}" }

  describe '#add_comment' do
    it 'calls post_request with expected params' do
      expect(described_class).to receive(:post_request).with("#{noteable_path}/notes", { body: text }, append_source_link: false)

      described_class.add_comment(text, noteable_path, append_source_link: false)
    end
  end

  describe '#update_discussion_comment' do
    it 'calls put_request with expected params' do
      expect(described_class).to receive(:put_request).with("#{discussion_path}/notes/#{note_id}", { body: text })

      described_class.update_discussion_comment(text, discussion_id, note_id, noteable_path)
    end
  end

  describe '#add_discussion' do
    it 'calls post_request with expected params' do
      expect(described_class).to receive(:post_request).with("#{noteable_path}/discussions", { body: text }, append_source_link: true)

      described_class.add_discussion(text, noteable_path, append_source_link: true)
    end
  end

  describe '#append_discussion' do
    it 'calls post_request with expected params' do
      expect(described_class).to receive(:post_request).with("#{discussion_path}/notes", { body: text }, append_source_link: false)

      described_class.append_discussion(text, discussion_id, noteable_path, append_source_link: false)
    end
  end

  describe '#merge_merge_request' do
    it 'calls put_request with expected params' do
      expect(described_class).to receive(:put_request).with("#{noteable_path}/merge", { merge_when_pipeline_succeeds: true })

      described_class.merge_merge_request(noteable_path)
    end
  end

  describe '#resolve_discussion' do
    it 'calls put_request with expected params' do
      expect(described_class).to receive(:put_request).with(discussion_path, { resolved: true })

      described_class.resolve_discussion(discussion_id, noteable_path)
    end
  end

  describe '#unresolve_discussion' do
    it 'calls put_request with expected params' do
      expect(described_class).to receive(:put_request).with(discussion_path, { resolved: false })

      described_class.unresolve_discussion(discussion_id, noteable_path)
    end
  end

  describe '.post_request' do
    it 'performs a POST request' do
      expect_api_request(verb: :post, path: path, request_body: { body: text }) do
        expect(described_class.post_request(path, { body: text })).to eq("POST #{Triage.api_endpoint(:com)}#{path}, params: `#{{ body: text }}`")
      end
    end

    it 'performs a POST request and appends source link' do
      source_path = 'some_automation.rb'
      expected_text = add_automation_suffix(source_path, text)
      expect(described_class).to receive(:get_source_path).and_return(source_path)

      expect_api_request(verb: :post, path: path, request_body: { body: expected_text }) do
        expect(described_class.post_request(path, { body: text }, append_source_link: true)).to eq("POST #{Triage.api_endpoint(:com)}#{path}, params: `#{{ body: expected_text }}`")
      end
    end

    context 'when source path cannot be determined' do
      it 'performs a POST request with no suffix' do
        expect(described_class).to receive(:get_source_path).and_return(nil)

        expect_api_request(verb: :post, path: path, request_body: { body: text }) do
          expect(described_class.post_request(path, { body: text }, append_source_link: true)).to eq("POST #{Triage.api_endpoint(:com)}#{path}, params: `#{{ body: text }}`")
        end
      end
    end
  end

  describe '.put_request' do
    it 'performs a PUT request' do
      expect_api_request(verb: :put, path: path, request_body: { body: text }) do
        expect(described_class.put_request(path, { body: text })).to eq("PUT #{Triage.api_endpoint(:com)}#{path}, params: `#{{ body: text }}`")
      end
    end
  end
end
