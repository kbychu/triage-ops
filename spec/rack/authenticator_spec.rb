# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/authenticator'

describe Triage::Rack::Authenticator do
  let(:fake_app) { double }
  let(:app)      { described_class.new(fake_app) }
  let(:env)      { {} }
  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  context 'when webhook_token is empty' do
    before do
      stub_env('GITLAB_WEBHOOK_TOKEN' => '')
    end

    it 'returns a 400 error' do
      expect(status).to eq(400)
      expect(body).to eq(JSON.dump(status: :unauthenticated, message: "Token wasn't provided"))
    end
  end

  context 'when not authenticated' do
    it 'returns a 401 error' do
      response

      expect(status).to eq(401)
      expect(body).to eq(JSON.dump(status: :unauthenticated))
    end
  end

  context 'when authenticated' do
    let(:env) { { 'HTTP_X_GITLAB_TOKEN' => 'foo' } }

    before do
      stub_env('GITLAB_WEBHOOK_TOKEN' => 'foo')
    end

    it 'calls the rest of the middleware stack' do
      expect(fake_app).to receive(:call).with(env)

      response
    end
  end
end
