# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/threat_insights/tag_author_if_ready_for_dev_without_weight'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::TagAuthorIfReadyForDevWithoutWeight do
  include_context 'with event', Triage::IssueEvent do
    let(:group_label) { 'group::threat insights' }
    let(:workflow_label) { 'workflow::ready for development' }
    let(:type_label) { 'type::feature' }
    let(:resource_open) { true }
    let(:from_gitlab_org) { true }
    let(:weight) { nil }
    let(:frontend_backend_labels) { %w[frontend backend] }

    let(:event_attrs) do
      {
        label_names: [group_label, workflow_label, type_label] + frontend_backend_labels,
        resource_open?: resource_open,
        from_gitlab_org?: from_gitlab_org,
        weight: weight
      }
    end
  end

  subject { described_class.new(event) }

  it_behaves_like 'processor documentation is present'

  include_examples 'registers listeners', ['issue.update']

  context 'when workflow label is "ready for development"' do
    let(:workflow_label) { 'workflow::ready for development' }

    include_examples 'event is applicable'

    context 'when not from gitlab.org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when only frontend label is present' do
      let(:frontend_backend_labels) { ['frontend'] }

      include_examples 'event is applicable'
    end

    context 'when only backend label is present' do
      let(:frontend_backend_labels) { ['backend'] }

      include_examples 'event is applicable'
    end

    context 'when neither backend nor frontend label is present' do
      let(:frontend_backend_labels) { [] }

      include_examples 'event is not applicable'
    end

    context 'when issue is not open' do
      let(:resource_open) { false }

      include_examples 'event is not applicable'
    end

    context 'when group label is not "threat insights"' do
      let(:group_label) { 'group::compliance' }

      include_examples 'event is not applicable'
    end

    context 'when type label is bug' do
      let(:type_label) { 'type::bug' }

      include_examples 'event is not applicable'
    end

    context 'when type label is spike' do
      let(:type_label) { 'spike' }

      include_examples 'event is not applicable'
    end

    context 'when weight is present' do
      let(:weight) { 2 }

      include_examples 'event is not applicable'
    end
  end

  context 'when workflow label is not "ready for development"' do
    let(:workflow_label) { 'workflow::refinement' }

    include_examples 'event is not applicable'
  end

  describe '#process' do
    it 'posts a comment' do
      expected_body = <<~MARKDOWN.chomp
        @#{event.event_actor_username}, please add the [weight as per the documentation](https://about.gitlab.com/handbook/engineering/development/sec/govern/sp-ti-planning.html#weights).

        ~"group::threat insights" issues require a weight when labelled ~"workflow::ready for development". If indeed this issue does not need a weight, then it must be either (labelled as a ~spike or ~bug), or (unlabelled as ~frontend and ~backend).
      MARKDOWN

      expect_comment_request(event: event, body: expected_body) do
        subject.process
      end
    end
  end
end
