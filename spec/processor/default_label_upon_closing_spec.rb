# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/default_label_upon_closing'
require_relative '../../triage/triage/event'

RSpec.describe Triage::DefaultLabelUponClosing do
  include_context 'with event', Triage::IssuableEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        type_label_set?: false
      }
    end

    let(:label_names) { %w[security] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.close', 'merge_request.close', 'merge_request.merge']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when type label is already set' do
      before do
        allow(event).to receive(:type_label_set?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when applicable' do
      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'calls default_label_upon_closing' do
      expect(subject).to receive(:default_label_upon_closing)

      subject.process
    end

    context "when direction is present" do
      let(:label_names) { %w[direction] }

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"type::feature"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context "when security is present" do
      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"type::maintenance"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context "when documentation is present" do
      let(:label_names) { %w[documentation] }

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"type::maintenance"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    # ~"type::feature" should be applied before ~"type::maintenance" in this case
    #
    # see https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1028 for context
    context 'when direction and security are present' do
      let(:label_names) { %w[direction security] }

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"type::feature"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
