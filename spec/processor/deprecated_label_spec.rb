# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/deprecated_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::DeprecatedLabel do
  include_context 'with event', Triage::IssuableEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'merge',
        from_gitlab_org?: true,
        added_label_names: added_label_names
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.close", "issue.note", "issue.open", "issue.reopen", "issue.update", "merge_request.approval", "merge_request.approved", "merge_request.close", "merge_request.merge", "merge_request.note", "merge_request.update", "merge_request.open", "merge_request.reopen", "merge_request.unapproval", "merge_request.unapproved"]

  describe '#applicable?' do
    context 'when there is no deprecated label' do
      let(:added_label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end

    context 'when event project is not under gitlab-org' do
      let(:added_label_names) { ['Manage [DEPRECATED]'] }

      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    described_class::DEPRECATED_LABEL_REPLACEMENT.each_key do |deprecated_label|
      context 'when there is a deprecated label' do
        let(:added_label_names) { [deprecated_label] }

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    described_class::DEPRECATED_LABEL_REPLACEMENT.each do |deprecated_label, new_label|
      context 'when there is a deprecated label' do
        let(:added_label_names) { [deprecated_label] }

        it 'posts a replacement message' do
          body = add_automation_suffix('processor/deprecated_label.rb') do
            <<~MARKDOWN.chomp
              Hey @root, please use ~"#{new_label}" as ~"#{deprecated_label}" has been deprecated.
              /unlabel ~"#{deprecated_label}"
              /label ~"#{new_label}"
            MARKDOWN
          end

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end

    context 'when multiple deprecated labels added' do
      let(:deprecated_labels) { ['Manage [DEPRECATED]', 'Create [DEPRECATED]'] }
      let(:added_label_names) { deprecated_labels + ['valid label'] }
      let(:replacement_labels) { ['devops::manage', 'devops::create'] }

      it 'posts a replacement message for deprecated labels only' do
        body = add_automation_suffix('processor/deprecated_label.rb') do
          <<~MARKDOWN.chomp
            Hey @root, please use #{replacement_labels.map { |l| %(~"#{l}") }.join(' ')} as #{deprecated_labels.map { |l| %(~"#{l}") }.join(' ')} has been deprecated.
            /unlabel #{deprecated_labels.map { |l| %(~"#{l}") }.join(' ')}
            /label #{replacement_labels.map { |l| %(~"#{l}") }.join(' ')}
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
