# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/processor/scheduled_issue_type_label_nudger'
require_relative '../../triage/triage/event'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::ScheduledIssueTypeLabelNudger do
  include_context 'with event', Triage::IssueEvent do
    let(:added_milestone_id) { 123 }
    let(:date_today)   { Date.today }
    let(:today)        { date_today.to_s }
    let(:in_30_days)   { (date_today + 30).to_s }

    let(:event_attrs) do
      { from_part_of_product_project?: true,
        added_milestone_id: added_milestone_id }
    end

    let(:milestone_attr) do
      { 'id' => added_milestone_id,
        'project_id' => project_id,
        'start_date' => today,
        'due_date' => in_30_days }
    end

    let(:issue_attr) do
      { 'iid' => iid, 'milestone' => milestone_attr, 'labels' => label_names }
    end
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}",
      response_body: issue_attr
    )

    stub_api_request(
      path: "/projects/#{project_id}/milestones/#{added_milestone_id}",
      response_body: milestone_attr
    )

    stub_api_request(
      path: "/projects/#{project_id}/issues/#{iid}/notes",
      query: { per_page: 100 },
      response_body: []
    )
  end

  include_examples 'registers listeners', ['issue.open', 'issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event did not change milestone_id' do
      let(:added_milestone_id) { nil }

      include_examples 'event is not applicable'
    end

    context 'when event changed milestone_id' do
      let(:issue_validator) { subject.__send__(:validator) }

      context 'when issue does not need type label nudge' do
        before do
          allow(issue_validator).to receive(:type_label_nudge_needed?).and_return(false)
        end

        include_examples 'event is not applicable'
      end

      context 'when issue needs type label nudge' do
        before do
          allow(issue_validator).to receive(:type_label_nudge_needed?).and_return(true)
        end

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules a ScheduledIssueTypeLabelNudgerJob 5 minutes later' do
      expect(Triage::ScheduledIssueTypeLabelNudgerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)
      subject.process
    end
  end
end
