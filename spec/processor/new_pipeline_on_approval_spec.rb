# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/new_pipeline_on_approval'
require_relative '../../triage/triage/event'

RSpec.describe Triage::NewPipelineOnApproval do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:approver_username) { 'approver' }
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org_gitlab?: true,
        team_member_author?: true,
        automation_author?: false,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        event_actor_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  subject { described_class.new(event) }

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  let(:merge_request_notes) do
    [
      { body: "review comment 1" },
      { body: "review comment 2" }
    ]
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)

    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', ['merge_request.approval', 'merge_request.approved']

  describe '#applicable?' do
    context 'when event is from gitlab-org/terraform-provider-gitlab' do
      let(:project_id) { Triage::Event::TF_PROVIDER_GITLAB_PROJECT_ID }

      include_examples 'event is applicable'
    end

    context 'when event is not from gitlab-org/gitlab or gitlab-org/terraform-provider-gitlab' do
      let(:project_id) { 1234 }

      include_examples 'event is not applicable'
    end

    context 'when MR has pipeline::expedite label' do
      let(:label_names) { ['pipeline:expedite'] }

      include_examples 'event is not applicable'
    end

    context 'when merge request source branch is `release-tools/update-gitaly`' do
      before do
        allow(event).to receive(:source_branch_is?).with(described_class::UPDATE_GITALY_BRANCH).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request target branch is a stable branch' do
      before do
        allow(event).to receive(:target_branch_is_stable_branch?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only qa, docs, and templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            },
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains a mixture of docs and non-docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end

    context 'when merge request contains a mixture of qa and non-qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'triggering a new pipeline' do
      it 'posts a comment to inform that a new pipeline will be triggered and schedules a new pipeline 1 minute later' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request has been approved. To ensure full test coverage, a new pipeline will be started shortly.

          For more info, please refer to the following links:
          - [`rspec` predictive jobs](https://docs.gitlab.com/ee/development/pipelines/index.html#rspec-predictive-jobs)
          - [`jest` predictive jobs](https://docs.gitlab.com/ee/development/pipelines/index.html#jest-predictive-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).

          /label ~"pipeline:mr-approved"
        MARKDOWN

        expect(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in).with(described_class::FIVE_SECONDS, event.noteable_path)

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when merge request author is a gitlab-org member' do
      it_behaves_like 'triggering a new pipeline'
    end

    context 'when merge request author is automation bot' do
      before do
        allow(event).to receive(:team_member_author?).and_return(false)
        allow(event).to receive(:automation_author?).and_return(true)
      end

      it_behaves_like 'triggering a new pipeline'
    end

    context 'when merge request author is not a gitlab-org member' do
      before do
        allow(event).to receive(:team_member_author?).and_return(false)
      end

      it 'posts a discussion to nudge the approver to start a new pipeline' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request has been approved. To ensure full test coverage, please start a new pipeline before merging.

          For more info, please refer to the following links:
          - [`rspec` predictive jobs](https://docs.gitlab.com/ee/development/pipelines/index.html#rspec-predictive-jobs)
          - [`jest` predictive jobs](https://docs.gitlab.com/ee/development/pipelines/index.html#jest-predictive-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).

          /label ~"pipeline:mr-approved"
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end

      context 'when project is outside of gitlab-org/gitlab' do
        let(:project_id) { Triage::Event::GITLAB_PROJECT_ID + 1 }

        include_examples 'event is not applicable'
      end
    end
  end
end
