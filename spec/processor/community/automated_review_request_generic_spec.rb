# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_generic'

RSpec.describe Triage::AutomatedReviewRequestGeneric do
  let(:group) { 'code review' }
  let(:distribution_project_id) { 42 }

  include_context 'with event', Triage::MergeRequestEvent do
    let(:label_names) { ['foo', "group::#{group}"] }
    let(:added_label_names) { [Labels::WORKFLOW_READY_FOR_REVIEW_LABEL] }
    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'MR Title' }
    let(:resource_author) { Triage::User.new(username: 'mr_author_username') }
    let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
    let(:targets_public_project) { true }

    let(:event_attrs) do
      {
        url: url,
        title: title,
        resource_author: resource_author,
        project_path_with_namespace: project_path_with_namespace,
        label_names: label_names
      }
    end
  end

  subject { described_class.new(event) }

  before do
    allow(WwwGitLabCom).to receive(:distribution_projects).and_return([distribution_project_id])
  end

  include_examples 'registers listeners', ["merge_request.update"]

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when "workflow::ready for review" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when project_id is a distribution project' do
      let(:project_id) { distribution_project_id }

      include_examples 'event is not applicable'
    end

    context 'when project_id is another project with an external review process' do
      let(:project_id) { 12006272 }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    include_context 'with discord posting context', 'DISCORD_WEBHOOK_PATH_COMMUNITY_MR_REQUEST_REVIEW'

    let(:coach_username) { '@coach_username' }
    let(:current_reviewers) { [] }

    let(:merge_request) do
      {
        project_id: event.project_id,
        iid: event.iid,
        reviewers: current_reviewers
      }
    end

    before do
      stub_api_request(path: "/projects/#{event.project_id}/merge_requests/#{event.iid}", response_body: merge_request)
      allow(subject).to receive(:select_random_merge_request_coach).with(group: group).and_return(coach_username)
      allow(event).to receive(:project_public?).and_return(targets_public_project)
      allow(discord_messenger_stub).to receive(:send_message)
    end

    shared_examples 'process merge request review request' do
      it 'posts a comment mentioning an MR coach to request a review' do
        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end

      context 'when posting messages to discord' do
        let(:expected_reviewers) do
          if current_reviewers.empty?
            "`#{coach_username.delete_prefix('@')}`"
          else
            "`#{current_reviewers.map { |user| user[:username] }.join('`, `')}`"
          end
        end

        before do
          allow(subject).to receive(:add_comment)
        end

        context 'when merge request has labels' do
          it_behaves_like 'discord message posting' do
            let(:message_body) do
              format_discord_message("`foo`, `group::#{group}`", expected_reviewers)
            end
          end
        end

        context 'when merge request has no labels' do
          let(:label_names) { [] }

          before do
            allow(subject).to receive(:select_random_merge_request_coach).with(group: nil).and_return(coach_username)
          end

          it_behaves_like 'discord message posting' do
            let(:message_body) do
              format_discord_message('none', expected_reviewers)
            end
          end
        end

        context 'when merge request targets a non public project' do
          let(:targets_public_project) { false }

          it_behaves_like 'no discord message posting'
        end

        it_behaves_like 'instantiates discord messenger'
      end
    end

    context 'when MR does not have any reviewer set' do
      it_behaves_like 'process merge request review request' do
        let(:body) do
          add_automation_suffix('processor/community/automated_review_request_generic.rb') do
            <<~MARKDOWN.chomp
              `#{coach_username}`, this ~"Community contribution" is ready for review.

              /assign_reviewer #{coach_username}

              - Do you have capacity and domain expertise to review this? We are mindful of your time, so if you are not
                able to take this on, please re-assign to one or more other reviewers.
              - Add the ~"workflow::in dev" label if the merge request needs action from the author.

              /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
            MARKDOWN
          end
        end
      end
    end

    context 'when MR already has reviewers set' do
      let(:current_reviewers) do
        [
          {
            id: 1,
            name: "John Doe",
            username: "john_doe"
          },
          {
            id: 2,
            name: "Jane Die",
            username: "jane_die"
          }
        ]
      end

      it_behaves_like 'process merge request review request' do
        let(:body) do
          add_automation_suffix('processor/community/automated_review_request_generic.rb') do
            <<~MARKDOWN.chomp
              `@john_doe` `@jane_die`, this ~"Community contribution" is ready for review.

              - Do you have capacity and domain expertise to review this? We are mindful of your time, so if you are not
                able to take this on, please re-assign to one or more other reviewers.
              - Add the ~"workflow::in dev" label if the merge request needs action from the author.

              /unlabel ~"#{Labels::AUTOMATION_AUTHOR_REMINDED_LABEL}"
            MARKDOWN
          end
        end
      end
    end
  end

  def format_discord_message(labels, reviewers)
    <<~TEXT
      A merge request from `#{event.resource_author.username}` is ready to be reviewed:
      - URL: #{event.url}
      - Title: `#{event.title}`
      - Project: `#{event.project_path_with_namespace}`
      - Labels: #{labels}
      - Reviewers: #{reviewers}
    TEXT
  end
end
