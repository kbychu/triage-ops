# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_request_review'

RSpec.describe Triage::CommandMrRequestReview do
  include_context 'with event', Triage::NoteEvent do
    let(:event_attrs) do
      {
        new_comment: new_comment,
        project_id: 123,
        iid: 456
      }
    end

    let(:merge_request_changes) do
      {
        'changes' => [
          {
            'old_path' => '.rubocop.yml',
            'new_path' => '.rubocop.yml',
            'diff' => '+ Enabled: true'
          }
        ]
      }
    end

    let(:new_comment) { %(#{Triage::GITLAB_BOT} ready) }
  end

  let(:by_team_member) { false }

  before do
    allow(Triage).to receive(:gitlab_org_group_member_usernames).and_return(['gl-reviewer1', 'gl-reviewer2', Triage::GITLAB_BOT.tr('@', '')])
    allow(event).to receive(:by_team_member?).and_return(by_team_member)
    stub_api_request(
      path: "/projects/123/merge_requests/456/changes",
      response_body: merge_request_changes)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', %w[ready review request_review] do
    let(:args_regex) { described_class::REVIEWERS_REGEX }
  end

  describe 'REVIEWERS_REGEX' do
    it { expect(described_class::REVIEWERS_REGEX).to eq(/@([^@[[:blank:]]]+)/) }
  end

  describe '#applicable?' do
    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution command processor #applicable?'

    context 'when actor is not a team member' do
      it_behaves_like 'rate limited', count: 1, period: 3600
    end

    context 'when actor is a team member' do
      let(:by_team_member) { true }

      it_behaves_like 'rate limited', count: 100, period: 3600
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:ready_action) { "/ready" }
    let(:label_action) { %(/label ~"#{Labels::WORKFLOW_READY_FOR_REVIEW_LABEL}") }

    context 'with no changes' do
      let(:merge_request_changes) { { 'changes' => [] } }

      it 'responds with no changes message and schedules label job' do
        body = <<~MARKDOWN.chomp
          #{ready_action}
          #{label_action}
        MARKDOWN
        expect(Triage::AddCommentJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event, body)

        expect_comment_request(event: event, body: add_automation_suffix('processor/community/command_mr_request_review.rb', subject.no_changes_message)) do
          subject.process
        end
      end
    end

    context 'with no reviewers given' do
      it 'sets ~"workflow::ready for review"' do
        body = <<~MARKDOWN.chomp
          #{ready_action}
          #{label_action}
        MARKDOWN
        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'with reviewers given' do
      shared_examples 'message posting' do |expected_reviewers:|
        it 'posts /assign_reviewer command' do
          expected_reviewers = Array(expected_reviewers)
          body = <<~MARKDOWN.chomp
            #{ready_action}
            #{label_action}
          MARKDOWN
          body += "\n/assign_reviewer #{expected_reviewers.join(' ')}" if expected_reviewers.any?

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end

      context 'when reviewer is @gl-reviewer1' do
        let(:new_comment) { "#{Triage::GITLAB_BOT} ready @gl-reviewer1" }

        it_behaves_like 'message posting', expected_reviewers: '@gl-reviewer1'
      end

      context 'with multiple reviewers on the same line with extra spaces' do
        let(:new_comment) { "#{Triage::GITLAB_BOT} ready   @gl-reviewer1    @gl-reviewer2" }

        it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1 @gl-reviewer2]
      end

      context 'with multiple commands on separate lines' do
        let(:new_comment) do
          <<~COMMENT
            Hello
            #{Triage::GITLAB_BOT} ready @gl-reviewer1
            #{Triage::GITLAB_BOT} ready @gl-reviewer2
          COMMENT
        end

        it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1]
      end

      # Regression test for https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1040
      context 'with command with mention on separate lines' do
        let(:new_comment) do
          <<~COMMENT
            Hello
            #{Triage::GITLAB_BOT} request_review

            #{Triage::GITLAB_BOT} foo"
          COMMENT
        end

        it_behaves_like 'message posting', expected_reviewers: %w[]
      end

      context 'with a mix of reviewers from the GitLab Team and not' do
        let(:new_comment) do
          <<~COMMENT
            Hello
            #{Triage::GITLAB_BOT} ready @gl-reviewer1 @not-gl-reviewer
          COMMENT
        end

        it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1 @not-gl-reviewer]
      end
    end
  end
end
