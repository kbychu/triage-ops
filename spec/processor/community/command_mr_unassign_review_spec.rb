# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_unassign_review'

RSpec.describe Triage::CommandMrUnassignReview do
  include_context 'with event', Triage::NoteEvent do
    let(:event_attrs) do
      {
        new_comment: new_comment
      }
    end

    let(:new_comment) { %(#{Triage::GITLAB_BOT} unassign_review) }
  end

  let(:by_resource_author) { true }

  let(:current_reviewers) do
    [{
      id: 1,
      name: "John Doe",
      username: event.event_actor_username
    }]
  end

  let(:merge_request) do
    {
      project_id: event.project_id,
      iid: event.iid,
      reviewers: current_reviewers
    }
  end

  before do
    stub_api_request(path: "/projects/#{event.project_id}/merge_requests/#{event.iid}", response_body: merge_request)
    allow(event).to receive(:by_resource_author?).and_return(by_resource_author)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', 'unassign_review'

  describe '#applicable?' do
    it_behaves_like 'rate limited', count: 100, period: 3600

    context 'when actor is not a reviewer' do
      let(:current_reviewers) { [] }

      it_behaves_like 'event is not applicable'
    end

    context 'when actor is a reviewer' do
      it_behaves_like 'event is applicable'
    end

    context 'when actor is not the resource author' do
      let(:by_resource_author) { false }

      it_behaves_like 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts /remove_reviewer command' do
      body = <<~MARKDOWN.chomp
        /remove_reviewer @#{event.event_actor_username}
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
