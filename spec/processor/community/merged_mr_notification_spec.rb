# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/merged_mr_notification'

RSpec.describe Triage::MergedMrNotification do
  include_context 'with slack posting context'
  include_context 'with event', Triage::NoteEvent do
    let(:event_attrs) do
      {
        title: 'Awesome feature!',
        url: 'https://mr.url/',
        noteable_author: Triage::User.new(username: username)
      }
    end

    let(:username) { 'agarcia' }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  include_examples 'registers listeners', ['merge_request.merge']
  include_examples 'processor slack options', '#mr-feedback'

  describe '#applicable?' do
    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution processor #applicable?'
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it_behaves_like 'slack message posting' do
      let(:message_body) do
        {
          text: <<~MARKDOWN
            `@#{username}`'s merge request #{event.title} was merged.
            #{event.url}
          MARKDOWN
        }
      end
    end
  end
end
