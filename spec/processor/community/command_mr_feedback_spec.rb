# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_feedback'

RSpec.describe Triage::CommandMrFeedback do
  include_context 'with slack posting context'
  include_context 'with event', Triage::NoteEvent do
    let(:event_attrs) do
      {
        new_comment: %(@gitlab-bot feedback #{comment}),
        url: 'https://comment.url/note#123',
        noteable_author: Triage::User.new(username: username)
      }
    end

    let(:comment) { 'awesome feedback' }
    let(:username) { 'agarcia' }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', 'feedback'
  include_examples 'processor slack options', '#mr-feedback'

  describe '#applicable?' do
    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution command processor #applicable?'
    it_behaves_like 'rate limited Slack message posting', count: 1, period: 86400
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it_behaves_like 'slack message posting' do
      let(:message_body) do
        {
          text: <<~MARKDOWN,
            Hola MR coaches, `@#{username}` has left feedback about their experience in #{event.url}.
          MARKDOWN
          attachments: [{ text: comment }]
        }
      end
    end
  end
end
