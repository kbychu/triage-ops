# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/team_label_inference'

RSpec.describe Triage::TeamLabelInference do
  include_context 'with event', Triage::IssuableEvent do
    let(:object_kind)            { 'issue' }
    let(:action)                 { 'open' }
    let(:from_gitlab_org)        { true }
    let(:gitlab_bot_event_actor) { false }
    let(:label_names)            { ['group::dynamic analysis'] }

    let(:event_attrs) do
      {
        object_kind: object_kind,
        action: action,
        from_gitlab_org?: from_gitlab_org,
        gitlab_bot_event_actor?: gitlab_bot_event_actor
      }
    end

    let(:comment_body_with_label_quick_action) do
      <<~MARKDOWN.chomp
        #{label_quick_action}
      MARKDOWN
    end

    let(:label_quick_action) { '/label ~"devops::plan" ~"section::dev"' }
  end

  shared_examples 'nothing happens' do
    it 'nothing happens' do
      expect_no_request do
        subject.process
      end
    end
  end

  subject { described_class.new(event) }

  before do
    stub_request(:get, "https://about.gitlab.com/sections.json")
      .to_return(status: 200, body: read_fixture('sections.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/stages.json")
      .to_return(status: 200, body: read_fixture('stages.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/groups.json")
      .to_return(status: 200, body: read_fixture('groups.json'), headers: {})

    stub_const("#{described_class}::UNSUPPORTED_DEVOPS_LABELS", ['devops::enablement'])
  end

  include_examples 'registers listeners', %w[issue.open issue.reopen issue.update merge_request.open merge_request.reopen merge_request.update]

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event was authored by gitlab bot' do
      let(:gitlab_bot_event_actor) { true }

      include_examples 'event is not applicable'
    end

    context 'with Section label' do
      let(:label_names) { %w[section::dev] }

      include_examples 'event is applicable'
    end

    context 'with Stage label' do
      let(:label_names) { %w[devops::create] }

      include_examples 'event is applicable'
    end

    context 'with Group label' do
      let(:label_names) { ['group::source code'] }

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when no Group, Stage or Section is present' do
      let(:action) { 'update' }
      let(:label_names) { [] }

      it_behaves_like 'nothing happens'
    end

    context 'when only Group is present' do
      context 'with supported group label' do
        let(:label_names) { ['group::dynamic analysis'] }
        let(:label_quick_action) { '/label ~"section::sec" ~"devops::secure"' }

        it 'infers Stage and Section labels' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported group label' do
        let(:label_names) { ['group::pricing'] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Stage is present' do
      let(:action) { 'reopen' }
      let(:label_names) { %w[devops::create] }
      let(:label_quick_action) { '/label ~"section::dev"' }

      context 'with supported Stage label' do
        it 'infers Section label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported Stage label' do
        let(:label_names) { %w[devops::enablement] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Section is present' do
      let(:label_names) { %w[section::ops] }

      it_behaves_like 'nothing happens'
    end

    context 'when only Stage and Group are present' do
      let(:object_kind) { 'merge_request' }
      let(:action) { 'update' }

      context 'when both group and Stage labels are unsupported' do
        let(:label_names) { %w[devops::enablement group::pricing] }

        it_behaves_like 'nothing happens'
      end

      context 'with unsupported group label' do
        let(:label_names) { %w[devops::create group::pricing] }
        let(:label_quick_action) { '/label ~"section::dev"' }

        it 'only infers the Section label from the supported Stage label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported Stage label' do
        let(:label_names) { %w[devops::enablement group::ide] }
        let(:label_quick_action) { '/label ~"section::dev" ~"devops::create"' }

        it 'infers both stage and section labels from the group label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group match' do
        let(:label_names) { %w[devops::create group::ide] }
        let(:label_quick_action) { '/label ~"section::dev"' }

        it 'only infers the Section label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group do not match' do
        let(:object_kind) { 'merge_request' }
        let(:action) { 'reopen' }
        let(:label_names) { %w[devops::deploy group::ide] }
        let(:label_quick_action) { '/label ~"section::dev" ~"devops::create"' }

        it 'infers the Stage from the Group and infers the Section from the Group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'when Section, Stage and Group and all are matching' do
      let(:label_names) { ['section::sec', 'devops::secure', 'group::dynamic analysis'] }

      it_behaves_like 'nothing happens'
    end

    context 'when Section, Stage and Group are present, but Stage label is unsupported' do
      let(:label_names) { ['section::sec', 'devops::enablement', 'group::dynamic analysis'] }
      let(:label_quick_action) { '/label ~"devops::secure"' }

      it 'sets the appropriate Stage label to align with Section and Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Stage is mismatched from Section and Group' do
      let(:label_names) { ['section::sec', 'devops::create', 'group::dynamic analysis'] }
      let(:label_quick_action) { '/label ~"devops::secure"' }

      it 'sets the appropriate Stage label to align with Section and Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Section and Stage are mismatched from Group' do
      let(:label_names) { ['section::ops', 'devops::create', 'group::project management'] }
      let(:label_quick_action) { '/label ~"devops::plan" ~"section::dev"' }

      it 'updates the Stage and Section to match the Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Group label is unsupported' do
      context 'when Stage and Section labels match' do
        let(:label_names) { ['section::dev', 'devops::create', 'group::pricing'] }

        it_behaves_like 'nothing happens'
      end

      context 'when Stage and Section labels do not match' do
        let(:label_names) { ['section::ops', 'devops::create', 'group::pricing'] }
        let(:label_quick_action) { '/label ~"section::dev"' }

        it 'infers section label from stage' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'when only Section and Group are present' do
      context 'with supported group label' do
        let(:label_names) { ['section::sec', 'group::dynamic analysis'] }
        let(:label_quick_action) { '/label ~"devops::secure"' }

        it 'infers the Stage from Group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported group label' do
        let(:label_names) { ['section::sec', 'group::pricing'] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Section and Stage are present' do
      context 'when they are matching' do
        let(:label_names) { %w[section::sec devops::secure] }

        it_behaves_like 'nothing happens'
      end

      context 'when they are mismatching' do
        context 'with unsupported Stage label' do
          let(:label_names) { %w[devops::enablement] }

          it_behaves_like 'nothing happens'
        end

        context 'with supported Stage label' do
          let(:label_names) { %w[section::dev devops::secure] }
          let(:label_quick_action) { '/label ~"section::sec"' }

          it 'the Stage takes precedence and updates the Section' do
            expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
              subject.process
            end
          end
        end
      end
    end

    context 'with label group::distribution"' do
      let(:label_names) { %w[group::distribution] }
      let(:label_quick_action) { '/label ~"section::enablement" ~"devops::systems"' }

      it 'adds Stage and Section labels' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'with label group::distribution:deploy"' do
      let(:label_names) { %w[group::distribution:deploy] }
      let(:label_quick_action) { '/label ~"section::enablement" ~"devops::systems"' }

      it 'adds Stage and Section labels' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'with label group::distribution and group::distribution:deploy"' do
      let(:label_names) { %w[section::enablement devops::systems group::distribution group::distribution::deploy] }

      it_behaves_like 'nothing happens'
    end

    context 'with label group::distribution and no subgroup name"' do
      let(:label_names) { %w[section::enablement devops::systems group::distribution] }

      it_behaves_like 'nothing happens'
    end

    context 'with label group::gitaly"' do
      let(:label_names) { %w[group::gitaly] }
      let(:label_quick_action) { '/label ~"section::enablement" ~"devops::systems"' }

      it 'adds Stage and Section labels' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'with label group::gitaly and group::gitaly::cluster"' do
      let(:label_names) { %w[section::enablement devops::systems group::gitaly group::gitaly::cluster] }

      it_behaves_like 'nothing happens'
    end

    context 'with label group::gitaly::cluster"' do
      let(:label_names) { %w[group::gitaly::cluster] }
      let(:label_quick_action) { '/label ~"section::enablement" ~"devops::systems"' }

      it 'adds Stage and Section labels' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'with invalid group name' do
      let(:label_names) { ['group::does not exist'] }

      it 'raises error' do
        expect { subject.process }.to raise_error(
          ArgumentError,
          "'does_not_exist' group is not found in groups JSON definition."
        )
      end
    end

    context 'with invalid Stage label' do
      let(:label_names) { ['devops::does not exist'] }

      it 'raises error' do
        expect { subject.process }.to raise_error(
          ArgumentError,
          "'does_not_exist' stage is not found in stages JSON definition."
        )
      end
    end

    context 'with Stage label that includes spaces' do
      let(:label_names) { ['devops::data stores'] }
      let(:label_quick_action) { '/label ~"section::enablement"' }

      it 'adds Section labels' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end
  end
end
