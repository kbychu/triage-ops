# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/closed_issue_workflow_label_updater_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ClosedIssueWorkflowLabelUpdaterJob do
  include_context 'with event', Triage::IssueEvent

  let(:request_body) do
    <<~MARKDOWN.chomp
      @root, the workflow label was automatically updated to ~"workflow::complete" because you closed the issue while in ~"workflow::verification".

      If this is not the correct label, please update.

      To avoid this message, update the workflow label as you close the issue.
      /label ~"workflow::complete"

      *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations).
      You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/job/closed_issue_workflow_label_updater_job.rb).*
    MARKDOWN
  end

  let(:issue_path)   { "/projects/#{project_id}/issues/#{iid}" }
  let(:issue_labels) { ['workflow::verification'] }
  let(:issue_state)  { 'closed' }

  let(:issue_attr) do
    {
      'project_id' => project_id,
      'iid' => iid,
      'labels' => issue_labels,
      'state' => issue_state
    }
  end

  before do
    stub_api_request(path: issue_path, response_body: issue_attr)
  end

  describe '#perform' do
    context 'when issue is closed and has workflow::verification label' do
      it 'posts comment and updates workflow label to workflow::complete' do
        expect_comment_request(event: event, body: request_body) do
          subject.perform(event)
        end
      end
    end

    context 'when issue is open' do
      let(:issue_state) { 'open' }

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: request_body },
          path: "/projects/#{project_id}/issues/#{iid}/notes",
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when issue has no workflow label' do
      let(:issue_labels) { [''] }

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: request_body },
          path: "/projects/#{project_id}/issues/#{iid}/notes",
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end

    context 'when issue has workflow::complete label' do
      let(:issue_labels) { ['workflow::complete'] }

      it 'does nothing' do
        expect_no_request(
          verb: :post,
          request_body: { body: request_body },
          path: "/projects/#{project_id}/issues/#{iid}/notes",
          response_body: {}
        ) do
          subject.perform(event)
        end
      end
    end
  end
end
