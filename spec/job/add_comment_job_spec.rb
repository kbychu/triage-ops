# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/add_comment_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::AddCommentJob do
  include_context 'with event', Triage::NoteEvent

  describe '#perform' do
    it 'adds the comment' do
      comment = 'comment'

      expect_comment_request(event: event, body: comment) do
        subject.perform(event, comment)
      end
    end
  end
end
